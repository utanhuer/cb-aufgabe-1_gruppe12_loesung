#include <stdio.h>
#include <stdlib.h>
#include "syntree.h"

int syntreeInit(Syntree* self) {
	self->blänge = 10;
	self->ncount = 0;
	self->node = malloc(10*sizeof(SyntreeNodeID));
	if(NULL == self->node) {
		fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
		free(self->node);
		return(1);
	}
	return 0;
}

void syntreeRelease(Syntree* self) {

	for(int i = 0; i<(self->ncount);i++) {
		if(((self->node[i]).nodetype) == ARRAY) {
			if(self->node[i].node.idlist.elements != NULL) {
			  free(self->node[i].node.idlist.elements);
			  self->node[i].node.idlist.elementcount = 0;
		    }
	     }
	 }
	free(self->node);
	self->ncount = 0;
	self->blänge = 0;
}

SyntreeNodeID syntreeNodeNumber(Syntree* self, int number) {
	checkTree(self);
	SyntreeNodeID newnode;
	SyntreeNodeID* existingnode;
	newnode.nodetype = INT;
	newnode.node.value = number;
	existingnode = findNode(self, newnode);
	if(existingnode == NULL) {
		if((self->ncount) == (self->blänge)) {
			self->node = realloc(self->node,2*(self->blänge)*sizeof(SyntreeNodeID));
			if(self->node == NULL) {
				fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
				syntreeRelease(self);
				exit(1);
			}
			self->blänge = 2*(self->blänge);
		}
		self->node[self->ncount] = newnode;
		self->ncount = (self->ncount)+1;
		return newnode;
	}
	else {
		return *existingnode;
	}
}

SyntreeNodeID syntreeNodeTag(Syntree* self, SyntreeNodeID id) {
	checkTree(self);
	static int nodename = 0;
	SyntreeNodeID newnode;
	SyntreeNodeID* existingnode;

	newnode.nodetype = ARRAY;
	newnode.node.idlist.elementcount = 0;
	newnode.node.idlist.elements = malloc(sizeof(SyntreeNodeID));
	newnode.node.idlist.name     = nodename++;
	if(newnode.node.idlist.elements == NULL) {
		fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
		syntreeRelease(self);
		exit(1);
	}

	existingnode = findNode(self, id);
	if(existingnode != NULL) {

   	  newnode.node.idlist.elements[newnode.node.idlist.elementcount] = *existingnode;
	  newnode.node.idlist.elementcount = (newnode.node.idlist.elementcount)+1;
	}

	if((self->ncount) == (self->blänge)) {
		self->node = realloc(self->node,2*(self->blänge)*sizeof(SyntreeNodeID));
		if(self->node == NULL) {
			fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
			syntreeRelease(self);
			exit(1);
		}
		self->blänge = 2*(self->blänge);
	}
	self->node[self->ncount] = newnode;
	self->ncount = (self->ncount)+1;
	return newnode;
}

SyntreeNodeID syntreeNodePair(Syntree* self, SyntreeNodeID id1, SyntreeNodeID id2) {
	checkTree(self);
	SyntreeNodeID newnode;
	SyntreeNodeID* existingnode = NULL;

	existingnode = findNode(self, id1);
	if(existingnode == NULL) {
		fprintf(stderr, "Das zu vereinigende Element id1 befindet sich nicht im Tree.\n");
		exit(1);
	}
	newnode = syntreeNodeTag(self, *existingnode);
	existingnode = findNode(self, id2);
	if(existingnode == NULL) {
		fprintf(stderr, "Das zu vereinigende Element id2 befindet sich nicht im Tree.\n");
		exit(1);
	}
	newnode = syntreeNodeAppend(self, newnode, *existingnode);

    return newnode;

}
// Hier machen !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SyntreeNodeID syntreeNodeAppend(Syntree* self, SyntreeNodeID list, SyntreeNodeID elem) {
	checkTree(self);
	SyntreeNodeID* existinglist = NULL;
	SyntreeNodeID* existingelem = NULL;

	existinglist = findNode(self, list);
	existingelem = findNode(self, elem);
	if(existinglist == NULL) {
		fprintf(stderr, "Das Element list befindet sich nicht im Tree.\n");
		exit(1);
	}
	if(existingelem == NULL) {
		fprintf(stderr, "Das Element elem befindet sich nicht im Tree.\n");
		exit(1);
	}
	existinglist->node.idlist.elements = realloc(existinglist->node.idlist.elements,
			                                     ((existinglist->node.idlist.elementcount)+1)*sizeof(SyntreeNodeID));
	if(existinglist->node.idlist.elements == NULL) {
		fprintf(stderr, "Nicht genug Speicher!\n");
		syntreeRelease(self);
		exit(1);
	}

	existinglist->node.idlist.elements[existinglist->node.idlist.elementcount] = *existingelem;
	existinglist->node.idlist.elementcount = (existinglist->node.idlist.elementcount)+1;
	return *existinglist;
}

SyntreeNodeID syntreeNodePrepend(Syntree* self, SyntreeNodeID elem, SyntreeNodeID list) {
	checkTree(self);
	SyntreeNodeID* existinglist = NULL;
	SyntreeNodeID* existingelem = NULL;

	existinglist = findNode(self, list);
	existingelem = findNode(self, elem);
	if(existinglist == NULL) {
		fprintf(stderr, "Das Element list befindet sich nicht im Tree.\n");
		exit(1);
	}
	if(existingelem == NULL) {
		fprintf(stderr, "Das Element elem befindet sich nicht im Tree.\n");
		exit(1);
	}

	existinglist->node.idlist.elements = realloc(existinglist->node.idlist.elements, ((existinglist->node.idlist.elementcount)+1)*sizeof(SyntreeNodeID));
	if(existinglist->node.idlist.elements == NULL) {
		fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
		syntreeRelease(self);
		exit(1);
	}
	for(int i = existinglist->node.idlist.elementcount; i>0; i--) {
		existinglist->node.idlist.elements[i] = existinglist->node.idlist.elements[i-1];
	}
	existinglist->node.idlist.elements[0] = *existingelem;
	existinglist->node.idlist.elementcount = (existinglist->node.idlist.elementcount)+1;
	return *existinglist;
}

void syntreePrint(const Syntree* self, SyntreeNodeID root) {
	checkTree(self);
	SyntreeNodeID* existingroot = NULL;

	existingroot = findNode(self, root);
	if(existingroot == NULL) {
		fprintf(stderr, "Das Root Element ist nicht im Baum Vorhanden.\n");
		exit(1);
	}
	if (existingroot->nodetype == INT) {
		printf("(%d)", existingroot->node.value);
		return;
	}
	printf("{");
	for(int i = 0; i<existingroot->node.idlist.elementcount; i++) {
		syntreePrint(self, existingroot->node.idlist.elements[i]);
	}
	printf("}");
}

SyntreeNodeID* findNode(const Syntree* self, SyntreeNodeID newnode) {
	if(newnode.nodetype == INT) {
		for(int i = 0; i<self->ncount; i++) {
			if(self->node[i].nodetype == INT) {
				if(self->node[i].node.value == newnode.node.value) {
					return &self->node[i];
				}
			}
		}
	}
	if(newnode.nodetype == ARRAY) {
		for(int i = 0; i<self->ncount; i++) {
			if(self->node[i].nodetype == ARRAY) {
				if(self->node[i].node.idlist.name == newnode.node.idlist.name) {
					return &self->node[i];
				}
			}
		}
	}
	return NULL;
}

void checkTree(const Syntree* self) {
	if(self->blänge == 0) {
		fprintf(stderr, "Der tree wurde nicht initialisiert.");
		exit(1);
	}
}