#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int stackInit(IntStack* self) {
	self->länge = 10;
	self->elemcount = 0;
	self->stack = malloc(10*sizeof(int));
	if(NULL == self->stack) {
		fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
		free(self->stack);
		return(1);
	}
	return 0;
}

void stackRelease(IntStack* self) {
	free(self->stack);
}

void stackPush(IntStack* self, int i) {
	if(self->länge == self->elemcount) {
		self->stack = realloc(self->stack, 2*(self->länge)*sizeof(int));
		if(NULL == self->stack) {
			fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
			free(self->stack);
			exit(1);
		}
		self->länge = 2*(self->länge);
	}
	self->stack[(self->elemcount)] = i;
	self->elemcount = (self->elemcount)+1;
}

int stackTop(const IntStack* self) {
	return self->stack[(self->elemcount)-1];
}

int stackPop(IntStack* self) {
	int stacktop = 0;
	if(stackIsEmpty(self)) {
		fprintf(stderr, "Der Stack ist leer!\n");
		free(self->stack);
		exit(1);
	}
	stacktop = self->stack[self->elemcount-1];
	self->elemcount = self->elemcount-1;
	if ((self->elemcount < ((self->länge)/4)) && (self->länge > 10)) {
		self->stack = realloc(self->stack, ((self->länge)/2)*sizeof(int));
		if(NULL == self->stack) {
			fprintf(stderr, "Kein Speicher mehr vorhanden!\n");
			free(self->stack);
			exit(1);
		}
		self->länge = (self->länge)/2;
	}
	return stacktop;

}

int stackIsEmpty(const IntStack* self) {
	if(self->elemcount == 0) {
		return 1;
	}
	return 0;
}

void stackPrint(const IntStack* self) {
	int counter = (self->elemcount)-1;
	while(counter >= 0) {
		printf("%d\n", self->stack[counter]);
		counter--;
	}
}